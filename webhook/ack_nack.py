"""Process all of the ACKs/NACKs that are associated with a merge request."""
import re
import sys

from cki_lib import logger
from cki_lib import misc
from gitlab.const import MAINTAINER_ACCESS

from . import cdlib
from . import common
from . import defs

LOGGER = logger.get_logger('cki.webhook.ack_nack')

APPROVAL_RULE_ACKS = (' - Approval Rule "%s" requires at least %d ACK(s) (%d given) '
                      'from set (%s).  \n')
APPROVAL_RULE_OKAY = ' - Approval Rule "%s" already has %d ACK(s) (%d required).  \n'
APPROVAL_RULE_OPT = ' - Approval Rule "%s" requests optional ACK(s) from set (%s).  \n'
STALE_APPROVALS_SUMMARY = ('Note: Approvals from %s have been invalidated due to code changes in '
                           'this merge request. Please re-approve, if appropriate.')


def _save(gl_instance, gl_project, gl_mergerequest, new_approval, create_gl_status_note,
          status, subsys_scoped_labels, message):
    # pylint: disable=too-many-arguments
    note = f'**ACK/NACK Summary: {status}**\n\n{message}'
    LOGGER.info(note)

    labels = [f'Acks::{status}'] + subsys_scoped_labels
    common.add_label_to_merge_request(gl_instance, gl_project, gl_mergerequest.iid, labels)

    if misc.is_production() and not common.mr_is_closed(gl_mergerequest):
        if new_approval is not None:
            gl_mergerequest.notes.create({'body': new_approval[1]})
        if create_gl_status_note:
            gl_mergerequest.notes.create({'body': note})


def _emails_to_gl_user_ids(gl_instance, reviewers):
    # Search GitLab for users with a given email address set.
    users = []
    for reviewer in reviewers:
        userlist = gl_instance.users.list(search=reviewer)
        for user in userlist:
            users.append(user.id)
    return users


def _ensure_base_approval_rule(gl_project, gl_mergerequest):
    """Ensure base approval rule exists and has right number of reviewers set."""
    if gl_mergerequest.state != "opened" or gl_mergerequest.draft:
        return

    proj_ar_list = gl_project.approvalrules.list(search='All Members')
    if len(proj_ar_list) < 1:
        LOGGER.warning("Project %s has no 'All Members' approval rule", gl_project.name)
        return

    proj_all_members = proj_ar_list[0]
    proj_rule_id = proj_all_members.id
    proj_rule_name = proj_all_members.name
    proj_reqs = proj_all_members.approvals_required
    if proj_reqs == 0:
        LOGGER.warning("Project %s isn't requiring any reviewers for MRs", gl_project.name)
        return

    ar_list = gl_mergerequest.approval_rules.list()
    reset_ar = False

    for approval_rule in ar_list:
        if approval_rule.name == 'All Members':
            if approval_rule.approvals_required == 0:
                # author access level, must be >= gitlab.MAINTAINER_ACCESS (40)
                aal = common.get_authlevel(gl_project, gl_mergerequest.author['id'])
                if gl_mergerequest.target_branch == "main":
                    LOGGER.warning("%s MR %d (branch: main) had approvals required set to 0",
                                   gl_project.name, gl_mergerequest.iid)
                    reset_ar = True
                elif aal < MAINTAINER_ACCESS:
                    LOGGER.warning("%s MR %d (branch: %s) had approvals required set to 0 by "
                                   "non-maintainer",
                                   gl_project.name, gl_mergerequest.iid,
                                   gl_mergerequest.target_branch)
                    reset_ar = True
                else:
                    LOGGER.debug("%s MR %d (branch: %s) has 'All Members' rule adjusted by "
                                 "maintainer",
                                 gl_project.name, gl_mergerequest.iid,
                                 gl_mergerequest.target_branch)
            else:
                LOGGER.debug("%s MR %d has 'All Members' rule set appropriately",
                             gl_project.name, gl_mergerequest.iid)

            if reset_ar:
                approval_rule.approvals_required = proj_reqs
                approval_rule.save()

            return

    LOGGER.warning("%s MR %d had no base approvals required",
                   gl_project.name, gl_mergerequest.iid)
    gl_mergerequest.approval_rules.create({'approval_project_rule_id': proj_rule_id,
                                           'name': proj_rule_name,
                                           'approvals_required': proj_reqs})


def _recheck_base_approval_rule(gl_mergerequest):
    ar_list = gl_mergerequest.approval_rules.list(search='All Members')
    branch = gl_mergerequest.target_branch

    for approval_rule in ar_list:
        if approval_rule.name == 'All Members':
            if approval_rule.approvals_required == 0 and branch == "main":
                gl_mergerequest.discussions.create({'body': '*ERROR*: MR has 0 required approvals'})
            return

    if not misc.is_production() or gl_mergerequest.draft:
        return

    gl_mergerequest.discussions.create({'body': '*ERROR*: MR has no "All Members" approval rule'})


def _edit_approval_rule(gl_instance, gl_mergerequest, subsystem, reviewers, num_req):
    """Add a GitLab Approval Rule named subsystem, with num_req required reviewers."""
    rule_exists = False
    rule = None
    current_rules = gl_mergerequest.approval_rules.list(as_list=False)
    for rule in current_rules:
        if rule.name == subsystem:
            rule_exists = True
            break

    if rule_exists:
        LOGGER.info("Approval rule for subsystem %s already exists", subsystem)
        return

    LOGGER.info("Create rule for ss %s, with %d required approval(s) from user(s) %s",
                subsystem, num_req, reviewers)

    user_ids = _emails_to_gl_user_ids(gl_instance, reviewers)
    LOGGER.debug("Approval Rule user ids: %s", user_ids)

    if not misc.is_production() or not user_ids:
        return

    gl_mergerequest.approvals.set_approvers(num_req, approver_ids=user_ids, approver_group_ids=[],
                                            approval_rule_name=subsystem)


def _get_reviewers(gl_instance, gl_project, gl_mergerequest, changed_files, owners_parser):
    """Parse the owners.yaml file and return a set of email addresses and subsystem labels."""
    # pylint: disable=too-many-branches,too-many-locals,too-many-arguments,too-many-statements
    if not changed_files:
        return [], []

    if gl_mergerequest.target_branch == "main-auto":
        return [], []

    commit_count, authlevel = common.get_commits_count(gl_project, gl_mergerequest)
    if commit_count > defs.MAX_COMMITS_PER_MR and authlevel < MAINTAINER_ACCESS:
        gl_mergerequest.notes.create({'body': '*ERROR*: This Merge Request has too many commits '
                                     f'for the approval webhook to process ({commit_count}) -- '
                                      'please make sure you have targeted the correct branch if '
                                      'you want reviewers automatically assigned.'})
        return [], []

    opt_reviewers = {}
    req_reviewers = {}
    all_reviewers = {}
    ar_num_req = 0
    # A merge request can span multiple subsystems so get the reviewers for each subsystem.
    # Call owners parser individually for each file.
    for changed_file in changed_files:
        for entry in owners_parser.get_matching_entries([changed_file]):
            ss_label = entry.get_subsystem_label()
            required_approvals = entry.get_required_approvals()
            # Get all maintainers and reviewers
            maintainers = entry.get_maintainers()
            reviewers = entry.get_reviewers()
            if maintainers is None or reviewers is None:
                LOGGER.warning("Got maintainers: %s, reviewers: %s, for entry %s",
                               maintainers, reviewers, entry)
                continue
            for user in [*maintainers, *reviewers]:
                if not user:
                    continue
                # Ensure that the merge request submitter doesn't show up in the reviewers list
                usernames = _emails_to_gl_user_names(gl_instance, [user['email']])
                if gl_mergerequest.author['username'] in usernames:
                    continue

                if ss_label not in opt_reviewers:
                    opt_reviewers[ss_label] = set([])
                    if required_approvals:
                        req_reviewers[ss_label] = set([])

                if ss_label not in all_reviewers:
                    all_reviewers[ss_label] = set([])

                all_reviewers[ss_label].update([user['email']])

                # Ensure that restricted users aren't listed as required reviewers
                if required_approvals and not user.get('restricted', False):
                    req_reviewers[ss_label].update([user['email']])
                else:
                    opt_reviewers[ss_label].update([user['email']])

    LOGGER.debug('Subsystem optional reviewers: %s', opt_reviewers)
    LOGGER.debug('Subsystem required reviewers: %s', req_reviewers)

    ss_reviewers = []
    for subsystem, reviewers in all_reviewers.items():
        ss_reviewers.append((reviewers, subsystem))

    for subsystem, reviewers in opt_reviewers.items():
        if reviewers:
            _edit_approval_rule(gl_instance, gl_mergerequest, subsystem, reviewers, ar_num_req)

    required = []
    ar_num_req = 1
    if req_reviewers:
        ar_list = gl_mergerequest.approval_rules.list()
        amappreq = 1
        for approval_rule in ar_list:
            if approval_rule.name == 'All Members':
                amappreq = approval_rule.approvals_required
                break
        authal = common.get_authlevel(gl_project, gl_mergerequest.author['id'])
        tbranch = gl_mergerequest.target_branch
        LOGGER.debug("access level = %d, target branch = %s, approvals required = %d",
                     authal, tbranch, amappreq)
        if authal >= MAINTAINER_ACCESS and tbranch != "main" and amappreq == 0:
            ar_num_req = 0

    for subsystem, reviewers in req_reviewers.items():
        required.append((reviewers, subsystem))
        if reviewers:
            _edit_approval_rule(gl_instance, gl_mergerequest, subsystem, reviewers, ar_num_req)

    return ss_reviewers, required


def get_ark_config_mr_ccs(merge_request):
    """Return a list of any CC email addresses from an ark project MR description."""
    cc_list = []
    if not merge_request.description:
        return cc_list
    mlines = merge_request.description.splitlines()
    for line in mlines:
        if line.startswith('Cc: ') and line.endswith('@redhat.com>'):
            cc_list.append(line.split()[-1].split('<')[1].split('>')[0])
    return cc_list


def get_ark_reviewers(project_id, merge_request, files):
    """Return the min number of reviews for the project and for ARK try to include reviewer set."""
    # For ark kernel config changes, also return users in th MR's CC line.
    if project_id == defs.ARK_PROJECT_ID and all(file.startswith('redhat/configs/')
                                                 for file in files):
        cc_reviewers = get_ark_config_mr_ccs(merge_request)
        if cc_reviewers:
            return [(set(cc_reviewers), None)]

    return None


def _validate_note_submitter(message, note_email, username):
    """Parse the message to make sure people aren't forging nacks via email bridge."""
    if username != defs.EMAIL_BRIDGE_ACCOUNT:
        return True

    bridged_email = None
    for line in message.split('\n'):
        pattern = " commented via email:"
        if not line.endswith(pattern):
            continue

        bridged_email = line.split(pattern)[0].split(' ')[-1].replace('<', '').replace('>', '')
        break

    if bridged_email != note_email:
        LOGGER.warning("Rejecting apparent forged nack attempt (%s != %s)",
                       bridged_email, note_email)
        return False

    return True


def _parse_tag(message):
    """Parse an individual message and look for any tags of interest."""
    # This allows the signoff email brackets (<>) to be escaped with a single backslash.
    regex = (r'(?P<tag>(Nacked-by|Re(scind|voke)-(N|n)acked-by)): '
             r'(?P<name>[^<\\]*) \\?<(?P<email>[^>\\]*)\\?>')
    tag_re = re.compile(regex, re.IGNORECASE)
    for line in message.split('\n'):
        match = tag_re.match(line)
        if not match:
            continue

        tag = match.group('tag')
        name = match.group('name')
        email = match.group('email')
        return tag, name, email

    return None, None, None


def _tag_email_error_message(gl_instance, tag, note_email, username):
    # cki-bot / cki-kwf-bot can impersonate other users for the approve/unapprove button.
    # redhat-patchlab leaves messages on behalf of users via the email bridge.
    if username in defs.BOT_ACCOUNTS:
        return None

    if not common.match_gl_username_to_email(gl_instance, note_email, username):
        return (f"Ignoring '{tag}: <{note_email}>' since this email doesn't match to "
                f"user @{username}'s account on GitLab. You can change "
                "this by clicking on your avatar at the top right and choosing Edit profile, "
                "then click on Emails on the left, or just click "
                f"[Edit Emails](https://gitlab.com/-/profile/emails), and then add {note_email} "
                "to your GitLab account's list of **Linked emails**, and go through the "
                "verification process so it shows as Verified, if this is really you.")

    return None


def _process_nacks(gl_instance, approval_data, messages):
    # pylint: disable=too-many-branches,too-many-locals,too-many-statements
    """Process a list of messages and collect any NACKS."""
    nacks = set([])
    valid_ack_nack = set([])
    # append in-flight approval/unapproval from button press
    if approval_data is not None:
        messages.append(approval_data)
    for message_timestamp, message, username in messages:
        (tag, name, note_email) = _parse_tag(message)
        if not tag:
            continue

        if not _validate_note_submitter(message, note_email, username):
            continue

        nack_valid = True
        tag_error = _tag_email_error_message(gl_instance, tag, note_email, username)
        if tag_error:
            LOGGER.warning(tag_error)
            nack_valid = False

        LOGGER.debug("Processing '%s: %s <%s>' ts=%s email=%s nack_valid=%s",
                     tag, name, note_email, message_timestamp, note_email, nack_valid)

        name_email = (name, note_email)
        if tag == 'Nacked-by' and nack_valid:
            nacks.add(name_email)
            valid_ack_nack.add(note_email)
        elif tag in ('Rescind-nacked-by', 'Revoke-nacked-by',
                     'Rescind-Nacked-by', 'Revoke-Nacked-by') and nack_valid:
            if name_email in nacks:
                nacks.remove(name_email)
                valid_ack_nack.remove(note_email)
            else:
                LOGGER.warning('Cannot find NACK to revoke for %s', name_email)
        else:
            LOGGER.warning("Unhandled tag: %s (from %s <%s>)", tag, name_email[0], name_email[1])

    return nacks


def _show_nacks(nacks):
    summary = [f'{nack[0]} <{nack[1]}>' for nack in nacks]
    summary.sort()
    count = len(summary)
    return ", ".join(summary), count


def _has_acknack(ack_nacks, subsystem_reviewers):
    for ack_nack in ack_nacks:
        if ack_nack[1] in subsystem_reviewers:
            return True

    return False


def _get_old_subsystems_from_labels(gl_mergerequest):
    subsys_list = []
    subsys_labels = []
    for label in gl_mergerequest.labels:
        if label.startswith('Acks::'):
            sslabel_parts = label.split("::")
            if len(sslabel_parts) == 3:
                subsys_list.append(sslabel_parts[1])
                subsys_labels.append(label)
    return subsys_list, subsys_labels


def _get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels):
    stale_labels = []
    for subsystem in old_subsystems:
        if subsystem not in subsys_scoped_labels.keys():
            for label in old_labels:
                if label.startswith(f"Acks::{subsystem}::"):
                    stale_labels.append(label)
    LOGGER.debug("Stale labels: %s", stale_labels)
    return stale_labels


def _get_ar_approval_status(gl_mergerequest, rule):
    current_approvals = gl_mergerequest.approvals.get(1)
    name = rule.name
    eligible_approvers = rule.eligible_approvers
    LOGGER.debug("Rule: %s, Eligible: %s (required: %d)",
                 name, rule.eligible_approvers, rule.approvals_required)
    req = rule.approvals_required
    given = 0
    for approver in current_approvals.approved_by:
        for eligible in rule.eligible_approvers:
            if approver['user']['id'] == eligible['id']:
                given += 1
                break
    approvers = []
    for approver in eligible_approvers:
        approvers.append(approver['username'])
    LOGGER.debug("Rule: %s, requires %s more approval(s) from set (%s )", name, req, approvers)

    LOGGER.info("Returning %s", given >= req)
    return given >= req


def _get_subsys_scoped_labels(gl_project, gl_mergerequest, nacks, req_reviewers):
    # pylint: disable=too-many-locals,too-many-arguments
    subsys_scoped_labels = {}
    (old_subsystems, old_labels) = _get_old_subsystems_from_labels(gl_mergerequest)

    if req_reviewers is not None:
        for subsystem_reviewers, subsystem_label in req_reviewers:
            LOGGER.info("Examining label %s", subsystem_label)
            mr_approval_rules = gl_mergerequest.approval_rules.list(as_list=False)
            subsystem_has_a_nack = _has_acknack(nacks, subsystem_reviewers)
            subsystem_approved = False
            for rule in mr_approval_rules:
                if rule.name == subsystem_label:
                    LOGGER.info("Getting approval status for %s", rule.name)
                    subsystem_approved = _get_ar_approval_status(gl_mergerequest, rule)
                    break

            if subsystem_has_a_nack:
                scoped_label = 'NACKed'
            elif not subsystem_approved:
                scoped_label = defs.NEEDS_REVIEW_SUFFIX
            else:
                scoped_label = defs.READY_SUFFIX

            if subsystem_label:
                subsys_scoped_labels[subsystem_label] = scoped_label

    stale_labels = _get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels)
    if stale_labels:
        common.remove_labels_from_merge_request(gl_project, gl_mergerequest.iid, stale_labels)

    ret = [f'Acks::{x}::{y}' for x, y in subsys_scoped_labels.items()]
    ret.sort()  # Sort the subsystem scoped labels for the tests.

    return ret


def _get_ar_approvals(gl_instance, gl_mergerequest, ar_reviewers, summary):
    # pylint: disable=too-many-locals,too-many-branches
    needed = []
    optional = []
    provided = []

    current_approvals = gl_mergerequest.approvals.get(1)
    if current_approvals and len(current_approvals.approved_by) > 0:
        summary.append("Approved by:\n")
        for approver in current_approvals.approved_by:
            summary.append(f" - {approver['user']['name']} @{approver['user']['username']}\n")
    else:
        summary.append("\n")

    req_approvals = 0
    for subsystem, required, given, _, user_ids in ar_reviewers:
        usernames = []
        for user_id in user_ids:
            usernames.append(f'@{gl_instance.users.get(user_id).username}')
        reviewers = list(usernames)
        reviewers.sort()
        reviewers = ', '.join(reviewers)
        if required > 0:
            if given >= required:
                provided.append(APPROVAL_RULE_OKAY % (subsystem, given, required))
            else:
                req_approvals += required - given
                needed.append(APPROVAL_RULE_ACKS % (subsystem, required, given, reviewers))
        elif given == 0:
            optional.append(APPROVAL_RULE_OPT % (subsystem, reviewers))
        else:
            provided.append(APPROVAL_RULE_OKAY % (subsystem, given, required))

    if needed:
        summary.append("\nRequired Approvals:  \n")
        summary += needed
    if optional:
        summary.append("\nOptional Approvals:  \n")
        summary += optional
    if provided:
        summary.append("\nSatisfied Approvals:  \n")
        summary += provided

    return summary, req_approvals


def _invalidate_nacks_via_approvals(gl_instance, gl_mergerequest, nacks):
    current_approvals = gl_mergerequest.approvals.get(1)
    if current_approvals and len(current_approvals.approved_by) > 0:
        for approver in current_approvals.approved_by:
            username = approver['user']['username']
            old_nacks = set([])
            for (name, email) in nacks:
                usernames = _emails_to_gl_user_names(gl_instance, [email])
                if username in usernames:
                    LOGGER.info("Rescinding nack from %s <%s>", name, email)
                    old_nacks.add((name, email))
            for (name, email) in old_nacks:
                nacks.remove((name, email))

    return nacks


def _nack_warn_if_existing_approval(gl_instance, gl_mergerequest, note_email):
    current_approvals = gl_mergerequest.approvals.get(1)
    if current_approvals and len(current_approvals.approved_by) > 0:
        for approver in current_approvals.approved_by:
            username = approver['user']['username']
            usernames = _emails_to_gl_user_names(gl_instance, [note_email])
            msg = f"@{username}, your nack will not count until you unapprove this MR too."
            if username in usernames:
                LOGGER.warning("Nack comment left, but user @%s has approved MR", username)
                gl_mergerequest.notes.create({'body': msg})


def _get_approval_summary(gl_instance, gl_project, gl_mergerequest, nacks,
                          req_reviewers, ar_reviewers):
    # pylint: disable=too-many-arguments,too-many-locals,too-many-branches
    summary = []
    labels = _get_subsys_scoped_labels(gl_project, gl_mergerequest, nacks, req_reviewers)

    summary, req_approvals = _get_ar_approvals(gl_instance, gl_mergerequest, ar_reviewers, summary)

    if nacks and summary:
        nacks = _invalidate_nacks_via_approvals(gl_instance, gl_mergerequest, nacks)
    (nack_summary, _) = _show_nacks(nacks)
    if nack_summary:
        summary.append(f'NACKed by {nack_summary}.')
        return 'NACKed', labels, ''.join(summary)

    if gl_project.only_allow_merge_if_all_discussions_are_resolved and \
       not gl_mergerequest.changes()['blocking_discussions_resolved']:
        summary.append('\nAll discussions must be resolved.')
        return defs.BLOCKED_SUFFIX, labels, ''.join(summary)

    if req_approvals > 0:
        return defs.NEEDS_REVIEW_SUFFIX, labels, ''.join(summary)

    approvals = gl_mergerequest.approvals.get(1)
    min_reviewers = approvals.approvals_required
    ack_count = min_reviewers - approvals.approvals_left
    if approvals.approvals_left and ack_count < min_reviewers:
        summary.append(f'\nRequires {min_reviewers - ack_count} more Approval(s).')
        return defs.NEEDS_REVIEW_SUFFIX, labels, ''.join(summary)

    if req_approvals > 0:
        summary.append(f'\nRequires {req_approvals} more Approval Rule Approval(s).')
        return defs.NEEDS_REVIEW_SUFFIX, labels, ''.join(summary)

    summary.append('\nMerge Request has all necessary Approvals.')
    LOGGER.info("Summary is:\n%s", summary)
    return defs.READY_SUFFIX, labels, ''.join(summary)


def _lookup_submitter_and_notes(gl_mergerequest):
    notes = []
    for note in gl_mergerequest.notes.list(sort='asc', order_by='created_at', as_list=False):
        notes.append((note.updated_at, note.body, note.author['username']))

    return notes


def _filter_stale_reviewers(gl_mergerequest, stale):
    # this should filter out reviewers added excplicity by non-bot users
    user_added = []
    bot_added = []
    notes = gl_mergerequest.notes.list(as_list=False)
    for note in notes:
        n_author = note.author['username']
        n_body = note.body
        if n_body.startswith("requested review from"):
            if n_author in defs.BOT_ACCOUNTS:
                for user in stale:
                    if f'@{user}' in n_body:
                        bot_added.append(user)
            else:
                for user in stale:
                    if f'@{user}' in n_body:
                        user_added.append(user)

    return [x for x in stale if x not in user_added and x in bot_added]


def _unassign_reviewers(gl_mergerequest, users):
    if users and misc.is_production() and not common.mr_is_closed(gl_mergerequest):
        LOGGER.info('Unassigning users %s to MR %s', users, gl_mergerequest.iid)

        # Assign the reviewers via a quick action to avoid race conditions.
        gl_mergerequest.notes.create({'body': '/unassign_reviewer ' + ' '.join(users)})


def _get_existing_reviewers(gl_mergerequest):
    return [x['username'] for x in gl_mergerequest.reviewers]


def _emails_to_gl_user_names(gl_instance, emails):
    # Search GitLab for users with a given email address set.
    users = set([])
    for email in emails:
        if "@redhat.com" not in email and "@fedoraproject.org" not in email:
            continue
        users.update([x.username for x in gl_instance.users.list(search=email)])
    return users


def _get_stale_reviewers(gl_instance, old, req_reviewers):
    reviewers = set([])
    for reviewer_set in req_reviewers:
        reviewers.update(reviewer_set[0])
    new = _emails_to_gl_user_names(gl_instance, reviewers)
    return [x for x in old if x not in new]


def _do_assign_reviewers(gl_mergerequest, users):
    if users and misc.is_production() and not common.mr_is_closed(gl_mergerequest):
        LOGGER.info('Assigning users %s to MR %s', users, gl_mergerequest.iid)

        # Assign the reviewers via a quick action to avoid race conditions.
        cmds = [f'/assign_reviewer @{x}' for x in users]
        gl_mergerequest.notes.create({'body': '\n'.join(cmds)})


def _assign_reviewers(gl_instance, gl_project, gl_mergerequest, req_reviewers):
    # Gather the list of expected reviewers/maintainers from owners.yaml and remove MR submitter.
    reviewers = set([])
    for reviewer_set in req_reviewers:
        reviewers.update(reviewer_set[0])

    users = _emails_to_gl_user_names(gl_instance, reviewers)
    LOGGER.info('Minimum reviewers on MR %s: %s', gl_mergerequest.iid, users)

    # Check the GitLab merge request reviewers field and remove users that are already assigned.
    for reviewer in gl_mergerequest.reviewers:
        if reviewer['username'] in users:
            users.remove(reviewer['username'])

    _do_assign_reviewers(gl_mergerequest, users)

    label = ['Reviewers::Wanted']
    approvals = gl_mergerequest.approvals.get(1)
    if not approvals.approvals_left:
        LOGGER.debug("Already approved, make sure it's not tagged for review")
        common.remove_labels_from_merge_request(gl_project, gl_mergerequest.iid, label)
        return

    # Count both gl_mergerequest.reviewers and users so we don't have to re-read gl_mergerequest
    # after adding someone from users that wasn't already there
    reviewer_count = len(gl_mergerequest.reviewers) + len(users)
    if reviewer_count < approvals.approvals_required:
        common.add_label_to_merge_request(gl_instance, gl_project, gl_mergerequest.iid, label)


def get_reviewers_from_approval_rules(gl_mergerequest):
    """Read the MR's approval rules for custom-added required reviewers."""
    mr_approval_rules = gl_mergerequest.approval_rules.list(as_list=False)
    current_approvals = gl_mergerequest.approvals.get(1)
    ar_reviewers = []
    for rule in mr_approval_rules:
        # skip our default All Members rules
        if rule.name == "All Members":
            continue
        name = rule.name
        eligible_approvers = rule.eligible_approvers
        LOGGER.debug("Rule: %s, Eligible: %s (required: %d)",
                     name, rule.eligible_approvers, rule.approvals_required)
        LOGGER.debug("Current approvals: %s", current_approvals.approved_by)
        req = rule.approvals_required
        given = 0
        for approver in current_approvals.approved_by:
            for eligible in rule.eligible_approvers:
                if approver['user']['id'] == eligible['id']:
                    given += 1
                    break
        approvers = []
        ids = []
        for approver in eligible_approvers:
            approvers.append(approver['username'])
            ids.append(approver['id'])
        LOGGER.debug("Rule: %s, requires %s more approval(s) from set (%s )", name, req, approvers)
        ar_reviewers.append([name, req, given, approvers, ids])

    return ar_reviewers


def process_merge_request(gl_instance, gl_project, gl_mergerequest, owners_parser, rhkernel_src,
                          create_gl_status_note, approval_data):
    # pylint: disable=too-many-arguments,too-many-locals
    """Process a merge request."""
    hook_name = "approval"
    run_on_drafts = False
    if common.do_not_run_hook(gl_project, gl_mergerequest, hook_name, run_on_drafts):
        return

    dep_label = cdlib.set_dependencies_label(gl_instance, gl_project, gl_mergerequest)
    if dep_label not in gl_mergerequest.labels:
        gl_mergerequest = gl_project.mergerequests.get(gl_mergerequest.iid)
    if dep_label == f"Dependencies::{defs.READY_SUFFIX}":
        changed_files = [change['new_path'] for change in gl_mergerequest.changes()['changes']]
    else:
        changed_files = cdlib.get_filtered_changed_files(gl_mergerequest)

    # Update the path_list to include corresponding Kconfig files
    changed_config_files, config_label = common.process_config_items(rhkernel_src, changed_files)
    changed_files.extend(changed_config_files)
    if config_label:
        common.add_label_to_merge_request(gl_instance, gl_project,
                                          gl_mergerequest.iid, config_label)
    LOGGER.debug('changed_files: %s', changed_files)

    # If get_ark_reviewers() returns any reviewers just use that as our reviewers set.
    reviewers = get_ark_reviewers(gl_project.id, gl_mergerequest, changed_files)
    required = None

    old_reviewers = _get_existing_reviewers(gl_mergerequest)
    LOGGER.debug("Old reviewers: %s", old_reviewers)

    _ensure_base_approval_rule(gl_project, gl_mergerequest)
    _recheck_base_approval_rule(gl_mergerequest)

    if not reviewers:
        reviewers, required = _get_reviewers(gl_instance, gl_project, gl_mergerequest,
                                             changed_files, owners_parser)
    LOGGER.debug("All reviewers: %s\nRequired reviewers: %s", reviewers, required)

    ar_reviewers = get_reviewers_from_approval_rules(gl_mergerequest)
    LOGGER.debug("Approval Rules Reviewers: %s", ar_reviewers)

    stale_reviewers = _get_stale_reviewers(gl_instance, old_reviewers, reviewers)
    if stale_reviewers:
        stale_reviewers = _filter_stale_reviewers(gl_mergerequest, stale_reviewers)
        LOGGER.debug("Stale reviewers: %s", stale_reviewers)
        _unassign_reviewers(gl_mergerequest, stale_reviewers)

    _assign_reviewers(gl_instance, gl_project, gl_mergerequest, reviewers)

    notes = _lookup_submitter_and_notes(gl_mergerequest)

    nacks = _process_nacks(gl_instance, approval_data, notes)

    LOGGER.debug('Changed files: %s', changed_files)
    LOGGER.debug('List of possible reviewers: %s', reviewers)
    LOGGER.debug('List of NACKs: %s', nacks)

    (_, report) = cdlib.mr_code_changed(gl_instance, gl_project, gl_mergerequest, rhkernel_src)
    if report:
        cdlib.reset_blocking_test_labels(gl_instance, gl_project, gl_mergerequest, owners_parser)

    summary = _get_approval_summary(gl_instance, gl_project, gl_mergerequest, nacks,
                                    required, ar_reviewers)
    _save(gl_instance, gl_project, gl_mergerequest, approval_data, create_gl_status_note, *summary)


def process_mr_webhook(gl_instance, msg, owners_parser, rhkernel_src, **_):
    """Process a merge request only if a label was changed."""
    approval_data = None
    gl_project = gl_instance.projects.get(msg.payload['project']['id'])
    if not (gl_mergerequest := common.get_mr(gl_project, msg.payload['object_attributes']['iid'])):
        return True

    create_gl_status_note = common.has_label_changed(msg.payload, 'Acks::', common.LabelPart.PREFIX)
    process_merge_request(gl_instance, gl_project, gl_mergerequest, owners_parser, rhkernel_src,
                          create_gl_status_note, approval_data)

    return True


def process_note_webhook(gl_instance, msg, owners_parser, rhkernel_src, **_):
    """Process a note message only if a tag was specified."""
    notetext = msg.payload['object_attributes']['note']
    create_gl_status_note = common.force_webhook_evaluation(notetext, 'ack-nack')

    gl_project = gl_instance.projects.get(msg.payload['project']['id'])
    if not (gl_mergerequest := common.get_mr(gl_project, msg.payload['merge_request']['iid'])):
        return

    (tag, _, note_email) = _parse_tag(notetext)
    if tag:
        tag_error = _tag_email_error_message(gl_instance, tag, note_email,
                                             msg.payload['user']['username'])
        if tag_error:
            LOGGER.warning(tag_error)
            if misc.is_production() and not common.mr_is_closed(gl_mergerequest):
                gl_mergerequest.notes.create({'body': tag_error})
            return
        if tag == "Nacked-by":
            _nack_warn_if_existing_approval(gl_instance, gl_mergerequest, note_email)
    elif not gl_mergerequest.blocking_discussions_resolved \
            and f'Acks::{defs.READY_SUFFIX}' in gl_mergerequest.labels:
        LOGGER.info('Processing because MR is blocked but has Acks::%s label', defs.READY_SUFFIX)
    elif gl_mergerequest.blocking_discussions_resolved \
            and f'Acks::{defs.BLOCKED_SUFFIX}' in gl_mergerequest.labels:
        LOGGER.info('Processing because MR is not blocked but has Acks::%s label',
                    defs.BLOCKED_SUFFIX)
    elif not create_gl_status_note:
        LOGGER.debug('Skipping note: %s', notetext)
        return

    process_merge_request(gl_instance, gl_project, gl_mergerequest, owners_parser, rhkernel_src,
                          create_gl_status_note, None)


WEBHOOKS = {
    'merge_request': process_mr_webhook,
    'note': process_note_webhook,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('ACK_NACK')
    parser.add_argument('--owners-yaml', **common.get_argparse_environ_opts('OWNERS_YAML'),
                        help='Path to the owners.yaml file')
    parser.add_argument('--rhkernel-src', **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory where rh kernel will be checked out')
    args = parser.parse_args(args)
    owners_parser = common.get_owners_parser(args.owners_yaml)
    common.generic_loop(args, WEBHOOKS, owners_parser=owners_parser, rhkernel_src=args.rhkernel_src)


if __name__ == '__main__':
    main(sys.argv[1:])
