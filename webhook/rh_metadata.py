"""Metadata for RHEL projects."""

from dataclasses import dataclass
from dataclasses import field
from dataclasses import fields
from dataclasses import replace

from cki_lib.misc import get_env_var_or_raise
from cki_lib.yaml import load


def check_data(this_dc):
    """Check the dataclass object fields have the right types and are not empty values."""
    dc_type = type(this_dc)
    for dc_field in fields(this_dc):
        fvalue = getattr(this_dc, dc_field.name)
        # fields should hold data of the correct type
        if not isinstance(fvalue, dc_field.type):
            raise TypeError(f'{dc_type}.{dc_field.name} must be {dc_field.type}: {this_dc}')
        # fields with non-empty default values should not have empty values
        if not isinstance(fvalue, bool) and dc_field.default != '' and not fvalue:
            raise ValueError(f'{dc_type}.{dc_field.name} cannot be empty: {this_dc}')


@dataclass(frozen=True)
class Branch:
    """Branch metadata."""

    name: str
    component: str
    distgit_ref: str
    internal_target_release: str = ''
    zstream_target_release: str = ''
    inactive: bool = False

    def __post_init__(self):
        """Check data."""
        check_data(self)


@dataclass(frozen=True)
class Project:
    """Project metadata."""

    ids: list
    name: str
    product: str
    branches: list
    inactive: bool = False

    def __post_init__(self):
        """Set up branches and check data."""
        self.__dict__['branches'] = [Branch(**b) for b in self.branches]
        check_data(self)

    def _return_branch(self, branch):
        """Return the branch with the inactive field set."""
        return branch if not branch or not self.inactive else replace(branch, inactive=True)

    def get_branch_by_name(self, branch_name):
        """Return the branch with the given name, or None."""
        return self._return_branch(next((b for b in self.branches if b.name == branch_name), None))

    def get_branches_by_itr(self, itr):
        """Return a list of active branches whose policy matches the given ITR."""
        # if the project is inactive then we treat all branches as inactive
        return [] if self.inactive else [b for b in self.branches if
                                         b.internal_target_release == itr and not b.inactive]

    def get_branches_by_ztr(self, ztr):
        """Return a list of active branches whose policy matches the given ZTR."""
        # if the project is inactive then we treat all branches as inactive
        return [] if self.inactive else [b for b in self.branches if
                                         b.zstream_target_release == ztr and not b.inactive]


@dataclass(frozen=True)
class Projects:
    """Projects metadata."""

    projects: list = field(init=False)

    def __post_init__(self):
        """Load yaml into Projects and check data."""
        projects_yaml = load(file_path=get_env_var_or_raise('RH_METADATA_YAML_PATH'))
        self.__dict__['projects'] = [Project(**p) for p in projects_yaml['projects']]
        check_data(self)

    def get_project_by_id(self, project_id):
        """Return the project with the given project_id, or None."""
        return next((p for p in self.projects if int(project_id) in p.ids), None)

    def get_project_by_name(self, project_name):
        """Return the project with the given name, or None."""
        return next((p for p in self.projects if p.name == project_name), None)
